FROM node as base

WORKDIR /app

COPY . /app

RUN npm install
###################################################
FROM node:10-alpine3.9

WORKDIR /app

COPY --from=base /app /app

EXPOSE 1337

CMD ["node" "app.js"]

